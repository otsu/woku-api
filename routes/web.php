<?php
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// Generate Applications Key
$router->get('key', function() {
    return Str::random(32);
});

$router->get('get', function(){
    return 'Hello, GET Method';
});

$router->post('post', function(){
    return 'Hello, POS Method';
});

$router->put('put', function(){
    return 'Hello, PUT Method';
});

$router->patch('patch', function(){
    return 'Hello, PATCH Method';
});


// Parameters
$router->get('users/{id}', function($id){
    return "User ID : {$id}";
});

// Multi Parameters
$router->get('posts/{postsId}/comments/{commensId}', function($postId, $commentsId){
    return "Post ID : {$postId} , Comment ID : {$commentsId}";
});

// Optional Parameters - bisa diisi atau tidak
$router->get('food[/{foodType}]', function($foodType = "rice"){
    return "Food : {$foodType}";
});

// Aliases
// mengkases url lain dengan mengakses nama alias router-nya
$router->get('alias-profile', function(){
    return redirect()->route('route.profile');
});

$router->get('profile/idstack', ['as' => 'route.profile', function(){
    return "Profile Aliases";
}]);

// Grouping url
$router->group(['prefix' => 'transaction'], function() use($router){
    $router->get('order', function(){
        return "Order Products";
    });
    $router->get('payment', function(){
        return "Payment Order";
    });
});

$router->group(['prefix' => 'user'], function() use($router){
    $router->get('registration', function(){
        return "User Registration";
    });
    $router->get('login', function(){
        return "User Login";
    });
});


// Middleware
$router->get('/dashboard',['middleware' => 'role', function(){
    return 'Welcome to Dashboard';
}]);

$router->get('role-unknown', function(){
    return 'Access denied';
});

// menghubungkan ke controller
$router->get('/datetime','CommonController@getDatetime');
$router->get('/products/category/{category}/price/{price}', 'CommonController@getProducts');

// tes class Request di controller
$router->get('/foo/{id}','CommonController@methodRequest');
$router->post('/member/profile','CommonController@memberProfile');

// authentication
$router->post('/register','AuthController@register');
$router->post('/login','AuthController@login');
$router->get('/profile/{id}', 'UserController@show');

// PRODUCTION

$router->group(['prefix' => 'customers'], function() use($router){
    $router->post('/register','CustomersController@register');
});