<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Customers extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    const id = 'id';
    const email = 'email';
    const password = 'password';
    const firstName = 'firstName';
    const lastName = 'lastName';
    const address = 'address';
    const mobile = 'mobile';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::id, self::email, self::password, self::firstName, self::lastName, self::address, self::mobile
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        self::password
    ];

    public function getEmail(){
        return $this->attributes[self::email];
    }

    public function setEmail($value){
         $this->attributes[self::email] = $value;
    }
 
    public function getFirstName(){
        return $this->attributes[self::firstName];
    }

    public function setFirstName($value){
         $this->attributes[self::firstName] = $value;
    }
    public function getLastName(){
        return $this->attributes[self::lastName];
    }

    public function setLastName($value){
         $this->attributes[self::lastName] = $value;
    }

    public function getAddress(){
        return $this->attributes[self::address];
    }

    public function setAddress($value){
        $this->attributes[self::address] = $value;
   }

    public function setMobile($value){
         $this->attributes[self::mobile] = $value;
    }

    public function getMobile(){
        return $this->attributes[self::mobile];
    }
}
