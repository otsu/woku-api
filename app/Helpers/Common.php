<?php 
namespace App\Helpers;

class Common{
    /**
     * check phone number
     * 
     * @param string $phoneNumber
     * @return boolean
     */
    public static function isPhoneFormatValid($phoneNumber){
        $valid = false;

        if (preg_match("/^(^\+62|62|^08)(\d{3,4}-?){2}\d{3,4}$/",$phoneNumber)) {
            $valid = true;
        }

        return $valid;
    }
    
}

?>