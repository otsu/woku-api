<?php

namespace App\Http\Controllers;

use \Illuminate\Http\Request;

class CommonController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('role',['only'=>['getProducts','getDatetime']]);
    }

    public function getDatetime(){
        date_default_timezone_set('Asia/Jakarta');
        return date('m/d/Y h:i:s a', time());
    }
    
    public function getProducts($category, $price){
        return "Product Category : {$category} and Price : {$price}";
    }

    public function methodRequest(Request $request,$Id){
        return $request->method().' - '.$Id;
    }

    public function memberProfile(Request $request){
        return $request->all();
    }
}
