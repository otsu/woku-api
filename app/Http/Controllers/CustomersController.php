<?php

namespace App\Http\Controllers;

use App\Http\Utilities\ResponseBuilder;
use App\Http\Validates\CustomersValidate;
use App\Models\Customers;
use Illuminate\Http\Request;

class CustomersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function register(Request $request){
        $class = (new CustomersValidate())->validateAll($request);

        if($class instanceof ResponseBuilder) return $class->send();

        return $class;
    }

    //
}
