<?php

namespace App\Http\Controllers;

use App\Http\Utilities\ResponseBuilder;
use App\Http\Utilities\ResponseCode;
use App\Http\Validates\AuthValidate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function register(Request $request){

        return (new AuthValidate())->register($request);
        

        // $user = New User(Input::all());

        // echo $user->getName().'   ';
        // $user->setName('Abdillah Hamzah');
        // echo $user->getName();
        

    
        /*
        try {
            $register = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password)            
            ]);
        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'Register Fail!',
                'data' => $e->getMessage()
            ], 422);
        }
        */
/*
        if($register){
            return response()->json([
                'success' => true,
                'message' => 'Register Succes!',
                'data' => $register
            ], 201);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Register Fail!',
                'data' => $register
            ], 404);
        }
        */
    }

    public function login(Request $request){
        $email = $request->email;
        $password = $request->password;

        $user = User::where('email',$email)->first();

        if(Hash::check($password, $user->password)){
            $api_token = base64_encode(Str::random(40));

            $user->update([
                'api_token' => $api_token
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Login succes!',
                'data' => [
                    'user' => $user,
                    'api_token' => $api_token
                ]
                ], 201);            
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Login fail!',
                'data' => ''
            ], 401);            
        }
    }
}
