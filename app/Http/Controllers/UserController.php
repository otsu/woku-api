<?php

namespace App\Http\Controllers;

use App\Models\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id){
        $result = User::find($id);

        if($result){
            return response()->json([
                'success' => true,
                'message' => 'User found!',
                'data' => $result
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'User not found!',
                'data' => ''
            ], 401);
        }
    }

    //
}
