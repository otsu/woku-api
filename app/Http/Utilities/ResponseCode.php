<?php
namespace App\Http\Utilities;

class ResponseCode{

    // SUCCESS
    public static $OK = 200;
    public static $CREATED = 201;
    public static $ACCEPTED = 202;
    public static $NOT_CONTENT = 204;

    // REDIRECTION
    public static $FOUND = 302;

    // CLIENT
    public static $BAD_REQUEST = 400;
    public static $UNAUTHORIZED = 401;
    public static $FORBIDDEN = 403;
    public static $NOT_FOUND = 404;
    public static $METHOD_NOT_ALLOWED = 405;
    public static $REQUEST_TIME_OUT = 408;
    public static $CONFLICT = 409;
    public static $UNPROCESSABLE_ENTITY = 422;

    // SERVER
    public static $BAD_GATEWAY = 502;
    public static $SERVICE_UNAVAILABLE = 503;
    public static $GATEWAY_TIMEOUT = 504;

    // APP
    public static $UNDEFINED_RESPONSE = -1;
    public static $RC_SUCCESS = "0000";
    public static $RC_NETWORK_INTERRUPTION = "0068";
    public static $RC_INVALID_TOKEN = "0051";
    public static $RC_UNPAID = "0011";
    public static $RC_ALREADY_PAID = "0012";
    public static $RC_ALREADY_REVERSAL = "0013";
    public static $RC_ERROR_SYSTEM = "0004";
    public static $RC_ERROR_DATABASE_SYSTEM = "0099";

    // DATABASE
    public static $DB_SYNTAX_ERROR = "42000";
    public static $DB_TABLE_VIEW_NOT_FOUND = "42S02";
    public static $DB_DB_NOT_FOUND = "1049";
    public static $DB_DUPLICATE_ENTRY = "23000";
    public static $DB_COLUMN_NOT_FOUND = "42S22";
    public static $DB_DATA_TOO_LONG = "22001";
    public static $DB_HOST_UNDEFINED = "2002";
    public static $DB_ACCESS_DENIED = "1045";

    // FPDI
    public static $PDF_DOC_ENCRYPTED = "268";

}
?>