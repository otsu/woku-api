<?php
namespace App\Http\Utilities;

class ResponseStatus{
    private static $response;

    public static function create($ResponseCode){
        switch($ResponseCode){
            case ResponseCode::$OK : 
                self::$response = self::toJson($ResponseCode,"Ok"); 
                break;
            case ResponseCode::$CREATED : 
                self::$response = self::toJson($ResponseCode,"Created"); 
                break;
            case ResponseCode::$ACCEPTED : 
                self::$response = self::toJson($ResponseCode,"Accepted"); 
                break;
            case ResponseCode::$NOT_CONTENT : 
                self::$response = self::toJson($ResponseCode,"No Content"); 
                break;
            case ResponseCode::$FOUND : 
                self::$response = self::toJson($ResponseCode,"Found"); 
                break;
            case ResponseCode::$BAD_REQUEST : 
                self::$response = self::toJson($ResponseCode,"Bad Request");                 
                break;
            case ResponseCode::$FORBIDDEN : 
                self::$response = self::toJson($ResponseCode,"Forbidden");                 
                break;
            case ResponseCode::$UNAUTHORIZED : 
                self::$response = self::toJson($ResponseCode,"Unauthorized");                 
                break;
            case ResponseCode::$NOT_FOUND : 
                self::$response = self::toJson($ResponseCode,"Not Found");                 
                break;
            case ResponseCode::$METHOD_NOT_ALLOWED : 
                self::$response = self::toJson($ResponseCode,"Method Not Allowed");                 
                break;
            case ResponseCode::$REQUEST_TIME_OUT : 
                self::$response = self::toJson($ResponseCode,"Request Time Out");                 
                break;
            case ResponseCode::$BAD_GATEWAY : 
                self::$response = self::toJson($ResponseCode,"Bad Gateway");                 
                break;
            case ResponseCode::$SERVICE_UNAVAILABLE : 
                self::$response = self::toJson($ResponseCode,"Service Unavailable");                 
                break;
            case ResponseCode::$GATEWAY_TIMEOUT : 
                self::$response = self::toJson($ResponseCode,"Gateway Timeout");                 
                break;
            case ResponseCode::$RC_SUCCESS : 
                self::$response = self::toJson($ResponseCode,"Success");                 
                break;
            case ResponseCode::$RC_INVALID_TOKEN : 
                self::$response = self::toJson($ResponseCode,"Token Invalid");                 
                break;
            case ResponseCode::$RC_NETWORK_INTERRUPTION : 
                self::$response = self::toJson($ResponseCode,"Network Interruption");                 
                break;
            case ResponseCode::$RC_UNPAID : 
                self::$response = self::toJson($ResponseCode,"Billing Unpaid");                 
                break;
            case ResponseCode::$RC_ALREADY_PAID : 
                self::$response = self::toJson($ResponseCode,"Billing Already Paid");                 
                break;
            case ResponseCode::$RC_ALREADY_REVERSAL : 
                self::$response = self::toJson($ResponseCode,"Billing Already Reversal");                 
                break;
            case ResponseCode::$RC_ERROR_SYSTEM : 
                self::$response = self::toJson($ResponseCode,"Error System");                 
                break;
            case ResponseCode::$CONFLICT : 
                self::$response = self::toJson($ResponseCode,"Conflict");                 
                break;
            case ResponseCode::$RC_ERROR_DATABASE_SYSTEM : 
                self::$response = self::toJson($ResponseCode,"Error Database System");                 
                break;
            case ResponseCode::$DB_DUPLICATE_ENTRY : 
                self::$response = self::toJson($ResponseCode,"Duplicate Entry");                 
                break;
            case ResponseCode::$DB_SYNTAX_ERROR : 
                self::$response = self::toJson($ResponseCode,"SQL Syntax Error");                 
                break;
            case ResponseCode::$DB_DB_NOT_FOUND : 
                self::$response = self::toJson($ResponseCode,"Database Not Found");                 
                break;            
            case ResponseCode::$DB_TABLE_VIEW_NOT_FOUND : 
                self::$response = self::toJson($ResponseCode,"Table or View Not Found");                 
                break;            
            case ResponseCode::$DB_COLUMN_NOT_FOUND : 
                self::$response = self::toJson($ResponseCode,"Column Not Found");                 
                break;            
            case ResponseCode::$DB_DATA_TOO_LONG : 
                self::$response = self::toJson($ResponseCode,"Data Too Long for Column");                 
                break;            
            case ResponseCode::$DB_HOST_UNDEFINED : 
                self::$response = self::toJson($ResponseCode,"Database Host Undefined");                 
                break;            
            case ResponseCode::$DB_ACCESS_DENIED : 
                self::$response = self::toJson($ResponseCode,"Database Access Denied");                 
                break;            
            case ResponseCode::$PDF_DOC_ENCRYPTED : 
                self::$response = self::toJson($ResponseCode,"PDF Document Encrypted");                 
                break;            
            case ResponseCode::$UNPROCESSABLE_ENTITY : 
                self::$response = self::toJson($ResponseCode,"Unprocessable Entity");                 
                break;            
            default : 
                self::$response = self::toJson($ResponseCode,"Undefined Response Status");                 
                break;                                                    
        }

        return self::$response;
    }

    private static function toJson($code,$status){
        return json_encode(["code" => $code, "status" => $status]);
    }
}
?>