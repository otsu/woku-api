<?php
namespace App\Http\Utilities;

class ResponseBuilder{
    // type of datetime
    private $timestamp;
    // type of ResponseStatus
    private $code;
    // type of ResponseStatus
    private $status;
    // type of string
    private $message;
    // type of data (plan / object)
    private $data;
    // type of string
    private $httpStatus;

    
    public function __construct(){
        $this->timestamp = date("d-m-Y H:i:s");
        $this->status = ResponseCode::$UNDEFINED_RESPONSE;
        $this->message = '';
        $this->data = null;
        $this->httpStatus = ResponseCode::$OK;
    }

    /**
     * Get the value of timestamp
     */ 
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set the value of timestamp
     *
     * @return  self
     */ 
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get the value of status
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */ 
    public function setStatus($status)
    {
        $codeStatus = json_decode(ResponseStatus::create($status));

        $this->status = $codeStatus->status;
        $this->setCode($codeStatus->code);

        return $this;
    }

    /**
     * Get the value of message
     */ 
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set the value of message
     *
     * @return  self
     */ 
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get the value of data
     */ 
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set the value of data
     *
     * @return  self
     */ 
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get the value of httpStatus
     */ 
    public function getHttpStatus()
    {
        return $this->httpStatus;
    }

    /**
     * Set the value of httpStatus
     *
     * @return  self
     */ 
    public function setHttpStatus($httpStatus)
    {
        $this->httpStatus = $httpStatus;

        return $this;
    }

    /**
     * Get the value of code
     */ 
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set the value of code
     *
     * @return  self
     */ 
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * get the value of all attributes
     *
     * @return  array of properties
     */ 
    public function getArray() {
        return get_object_vars($this); 
    }        

    /**
     * send response
     *
     * @return  json
     */ 
    public function send(){
        $messages = $this->getArray();
        unset($messages['httpStatus']);

        return response()->json($messages, $this->httpStatus);
    }

}
