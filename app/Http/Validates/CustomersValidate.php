<?php
namespace App\Http\Validates;

use App\Helpers\Common;
use App\Http\Controllers\Controller;
use App\Http\Utilities\ResponseBuilder;
use App\Http\Utilities\ResponseCode;
use App\Models\Customers;
use Illuminate\Validation\ValidationException;

class CustomersValidate extends Controller{
    /**
     * do validate for all 
     * 
     * @return class
     */
    public function validateAll($request){
        $class = $this->isParamsValid($request);
        if($class instanceof ResponseBuilder) return $class;

        $class = $this->isPhoneNumberValid($request);
        if($class instanceof ResponseBuilder) return $class;
        
        return $class;
    }

    /**
     * Check all parameters
     * 
     * @return class
     */
    public function isParamsValid($request){
        try {
            $validate = $this->validate($request, [
                Customers::email => 'required|email|unique:customers|max:50',
                Customers::password => 'required|max:30',
                Customers::firstName => 'required|alpha|max:30',
                Customers::lastName => 'required|alpha|max:30',
                Customers::address => 'required|max:50',
                Customers::mobile => 'required|numeric|digits_between:12,13'
            ]);

            return new Customers($request->all());

        } catch (ValidationException $e) {
            return (new ResponseBuilder())
                ->setStatus(ResponseCode::$BAD_REQUEST)
                ->setMessage($e->getMessage())
                ->setData($e->getResponse()->original)
                ->setHttpStatus(ResponseCode::$BAD_REQUEST);
        }
    }

    /**
     * check wether phone number is valid
     * 
     * @return boolean
     */
    public function isPhoneNumberValid($request){
        $customer = new Customers($request->all());
        if(Common::isPhoneFormatValid($customer->getMobile())){
            return new Customers($request->all());
        }else{
            return (new ResponseBuilder())
                ->setStatus(ResponseCode::$BAD_REQUEST)
                ->setMessage('Mobile/Phone Number formats invalid!')
                ->setData($request->all())
                ->setHttpStatus(ResponseCode::$BAD_REQUEST);
        }
    }
}

?>