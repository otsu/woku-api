<?php
namespace App\Http\Validates;

use App\Http\Controllers\Controller;
use App\Http\Utilities\ResponseBuilder;
use App\Http\Utilities\ResponseCode;
use Illuminate\Validation\ValidationException;

class AuthValidate extends Controller{
    public function register($request){
        try {
            $validate = $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email|unique:users'
            ]);

            return (new ResponseBuilder())
                ->setStatus(ResponseCode::$OK)
                ->setMessage('Register success!')
                ->setData($request->all())
                ->setHttpStatus(ResponseCode::$OK)
                ->send();
            die;

        } catch (ValidationException $e) {
            return (new ResponseBuilder())
                ->setStatus(ResponseCode::$BAD_REQUEST)
                ->setMessage('Register fail!')
                ->setData($e->getResponse())
                ->setHttpStatus(ResponseCode::$BAD_REQUEST)
                ->send();
            die;
        }
    }
}

?>